# SHIPSANTAMARTA

Shipping web application in the city of Santa Marta, Colombia.

### IMPORTANTE

   Para correr lee este articulo: https://www.bezkoder.com/spring-boot-jwt-authentication/
   
### Run FrontEnd and BackEnd

 - FrontEnd (Reactjs)

   First cd domsantamarta_backend
   
   1. npm install
   2. npm start

 - BackEnd (Spring) -> configurate resources/application.properties
   
   Firts cd domsantamarta_backend 
   
   1. mvn spring-boot:run

### Rutas de la Aplicación
 A continuación todas las rutas accesibles de la web app

- /domiciliary + /create/domiciliary -> Mensajeros

- / + /create/shipping -> Domicilios

- /account + /login + /register -> Usuarios
 
- /map -> Mapa

### ENDPOINTS API

 - Domiciliary

   - "api/mensajeros" -> GET(return all mensajeros) 
   - "api/mensajeros/:id" -> GET(return domiciliary by id)
   - "api/mensajeros" -> POST( create and return domiciliary)
   - "api/mensajeros/:id" -> PUT(update and return domiciliary by id)

 - Shipping
   - "api/domicilios?state=" -> GET( return all state=true(Open) or state=false(Close) or without state all)
   - "api/domicilios/coords?state=" -> GET( return all coords state=true(Open) or state=false(Close) or without state all coords)
   - "api/domicilios/:id" -> GET(return shipping by id)
   - "api/domicilios" -> POST( create and return shipping)
   - "api/domicilios/:id" -> PUT(update and return shipping by id)

 - User
   - "api/usuarios/:id" -> GET(return user by id)
   - "api/usuarios?emai=&password=" -> POST(login)
   - "api/usuarios/:id" -> PUT(update and return user by id)

### JSON de test - API Example -> Endpoints

- Domiciliary -> "api/mensajeros"

    {
        "nombre": "Lester",
        "direccion": "11.21521",
        "cedula": "150406062",
        "telefono": "2322632",
        "placa_moto": "ZYV95B"
    }

- domicilios  -> "api/domicilios"

     {
      "solicitante_nombre": "yyyysy",
      "solicitante_latitud": "41.1212",
      "solicitante_longitud": "12.12",
      "solicitante_telefono": "23232",
      "hora": "2pm",
      "destinatario_nombre": "xxxx",
      "destinatario_latitud": "12.12",
      "destinatario_longitud": "11.212",
      "destinatario_telefono": "123214",
      "descrip_paquete": "descrinsjanbdsds",
      "mensajero":1
    } 

- Users -> "api/usuarios"

   /api/auth/signup -> Registro

   {
    "username":"Lester",
    "email":"lester@gmail.com",
    "password":"lester1234",
    "role":["user","admin"]
   }
   
   /api/auth/signin

   {
    "username":"Lester",
    "password":"lester1234"
   }

