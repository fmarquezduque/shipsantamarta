package com.example.domsantamarta_backend.entity;


import java.util.Set;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name="mensajeros")
public class Mensajero implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_mensajero")
    private Long id;

    @Column(name = "nombre", nullable = false)
    private String nombre;

    @Column(name = "direccion", nullable = false)
    private String direccion;

    @Column(name = "cedula", nullable = false, unique = true)
    private String cedula;

    @Column(name = "telefono", nullable = false, unique = true )
    private String telefono;

    @Column(name = "placa_moto", nullable = false, unique = true)
    private String placa_moto;
    
    @OneToMany(mappedBy="mensajero") 
	private Set<Domicilio> domicilios;

    @Column(name="enabled", nullable = false)
	private Boolean enabled = true;

    @Column(name="created_at")
    @CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;

    public Mensajero() {
		super();
	}

    public Long getId() {
        return id;
    }
    public String getNombre() {
        return nombre;
    }
    public String getDireccion() {
        return direccion;
    }
    public String getCedula() {
        return cedula;
    }
    public String getTelefono() {
        return telefono;
    }
    public String getPlaca_moto() {
        return placa_moto;
    }
    public Set<Domicilio> getDomicilios() {
        return domicilios;
    }
    public Boolean getEnabled() {
        return enabled;
    }
    public Date getCreatedAt() {
        return createdAt;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public void setPlaca_moto(String placa_moto) {
        this.placa_moto = placa_moto;
    }
    public void setDomicilios(Set<Domicilio> domicilios) {
        this.domicilios = domicilios;
    }
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

}
