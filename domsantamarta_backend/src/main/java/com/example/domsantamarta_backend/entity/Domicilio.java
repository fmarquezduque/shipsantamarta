package com.example.domsantamarta_backend.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.ManyToOne;

@Entity
@Table(name="domcilios")
public class Domicilio implements Serializable {
    
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

    @Column(name = "solicitante_nombre", nullable = false)
    private String solicitante_nombre;

    @Column(name = "solicitante_latitud", nullable = false)
    private String solicitante_latitud;

    @Column(name = "solicitante_longitud", nullable = false)
    private String solicitante_longitud;

    @Column(name = "solicitante_telefono", nullable = false)
    private String solicitante_telefono;

    @Column(name = "hora", nullable = false)
    private String hora;

    @Column(name = "destinatario_nombre", nullable = false)
    private String destinatario_nombre;

    @Column(name = "destinatario_latitud", nullable = false)
    private String destinatario_latitud;

    @Column(name = "destinatario_longitud", nullable = false)
    private String destinatario_longitud;

    @Column(name = "destinatario_telefono", nullable = false)
    private String destinatario_telefono;
    
    @Column(name = "descrip_paquete", nullable = false)
    private String descrip_paquete;

    @Column(name = "id_mensajero")
    private Long mensajero;

    @Column(name="estado", nullable = false)
    private Boolean estado = true;

    @Column(name="created_at")
    @CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;

    public Domicilio() {
        super();
    }
    
    public Long getId() {
        return id;
    }

    public String getSolicitante_nombre() {
        return solicitante_nombre;
    }
    public String getSolicitante_latitud() {
        return solicitante_latitud;
    }
    public String getSolicitante_longitud() {
        return solicitante_longitud;
    }
    public String getSolicitante_telefono() {
        return solicitante_telefono;
    }
    public String getDestinatario_nombre() {
        return destinatario_nombre;
    }
    public String getDestinatario_latitud() {
        return destinatario_latitud;
    }
    public String getDestinatario_longitud() {
        return destinatario_longitud;
    }
    public String getDestinatario_telefono() {
        return destinatario_telefono;
    }
    public String getHora() {
        return hora;
    }
    public String getDescrip_paquete() {
        return descrip_paquete;
    }
    public Boolean getEstado() {
        return estado;
    }
    public Long getMensajero() {
        return mensajero;
    }
    public Date getCreatedAt() {
        return createdAt;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public void setSolicitante_nombre(String solicitante_nombre) {
        this.solicitante_nombre = solicitante_nombre;
    }
    public void setSolicitante_latitud(String solicitante_latitud) {
        this.solicitante_latitud = solicitante_latitud;
    }
    public void setSolicitante_longitud(String solicitante_longitud) {
        this.solicitante_longitud = solicitante_longitud;
    }
    public void setSolicitante_telefono(String solicitante_telefono) {
        this.solicitante_telefono = solicitante_telefono;
    }
    public void setHora(String hora) {
        this.hora = hora;
    }
    public void setDestinatario_nombre(String destinatario_nombre) {
        this.destinatario_nombre = destinatario_nombre;
    }
    public void setDestinatario_latitud(String destinatario_latitud) {
        this.destinatario_latitud = destinatario_latitud;
    }
    public void setDestinatario_longitud(String destinatario_longitud) {
        this.destinatario_longitud = destinatario_longitud;
    }
    public void setDestinatario_telefono(String destinatario_telefono) {
        this.destinatario_telefono = destinatario_telefono;
    }
    public void setMensajero(Long mensajero) {
        this.mensajero = mensajero;
    }
    public void setDescrip_paquete(String descrip_paquete) {
        this.descrip_paquete = descrip_paquete;
    }
    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

}
