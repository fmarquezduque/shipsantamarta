package com.example.domsantamarta_backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DomsantamartaBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(DomsantamartaBackendApplication.class, args);
	}

}
