package com.example.domsantamarta_backend.controllers;

import java.util.List;

import com.example.domsantamarta_backend.entity.Mensajero;
import com.example.domsantamarta_backend.repository.MensajeroRepository;

import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.security.access.prepost.PreAuthorize;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.PUT})
@RequestMapping("api/mensajeros")
public class MensajeroController {
    
    @Autowired
    private MensajeroRepository mensajeroRepository;

    @GetMapping
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public List<Mensajero> all(){

        List<Mensajero> mensajeros = mensajeroRepository.findAll();
        return mensajeros;
    }

    @PostMapping
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<?> create(@RequestBody Mensajero mensajero){
        
        Optional<Mensajero> oMensajero = mensajeroRepository.findMensajeroByCedulaOrPlacaMotoOrTelefono(mensajero.getCedula(), mensajero.getPlaca_moto(), mensajero.getTelefono());

        if(!oMensajero.isPresent()){
            return ResponseEntity.status(HttpStatus.CREATED).body(mensajeroRepository.save(mensajero));
        }

        return ResponseEntity.badRequest().body("Ya existe este mensajero en nuestros registros!");
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<?> mensajeroId(@PathVariable Long id){
        Optional<Mensajero> oMensajero  = mensajeroRepository.findById(id);

        if(!oMensajero.isPresent()){
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(oMensajero);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<?> update(@RequestBody Mensajero mensajeroUpdate, @PathVariable Long id){
        Optional<Mensajero> oMensajero  = mensajeroRepository.findById(id);

        if(!oMensajero.isPresent()){
            return ResponseEntity.notFound().build();
        }

        oMensajero.get().setNombre(mensajeroUpdate.getNombre());
        oMensajero.get().setDireccion(mensajeroUpdate.getDireccion());
        oMensajero.get().setCedula(mensajeroUpdate.getCedula());
        oMensajero.get().setTelefono(mensajeroUpdate.getTelefono());
        oMensajero.get().setPlaca_moto(mensajeroUpdate.getPlaca_moto());
        oMensajero.get().setEnabled(mensajeroUpdate.getEnabled());
 
        return ResponseEntity.status(HttpStatus.CREATED).body(mensajeroRepository.save(oMensajero.get()));
    }


}
