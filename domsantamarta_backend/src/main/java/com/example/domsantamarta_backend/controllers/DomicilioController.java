package com.example.domsantamarta_backend.controllers;

import java.util.List;
import java.util.Optional;

import com.example.domsantamarta_backend.entity.Domicilio;
import com.example.domsantamarta_backend.repository.DomicilioRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.security.access.prepost.PreAuthorize;    

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST, RequestMethod.PUT})
@RequestMapping("api/domicilios")
public class DomicilioController {
    
    @Autowired
    private DomicilioRepository domicilioRepository;
    
    @GetMapping
    @PreAuthorize("hasRole('USER')")
    public List<Domicilio> all(@RequestParam(required=false) Boolean state){
        List<Domicilio> Domicilios = domicilioRepository.findDomiciliosByEstado(state);
        return Domicilios;
    }

    @PostMapping
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> create(@RequestBody Domicilio Domicilio){
        return ResponseEntity.status(HttpStatus.CREATED).body(domicilioRepository.save(Domicilio));
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> domicilioId(@PathVariable Long id){
        Optional<Domicilio> oDomicilio  = domicilioRepository.findById(id);

        if(!oDomicilio.isPresent()){
           return  ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(oDomicilio);
    }
    
    @PutMapping("/{id}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> update(@RequestBody Domicilio domicilioUpdate, @PathVariable Long id){
        Optional<Domicilio> oDomicilio  = domicilioRepository.findById(id);

        if(!oDomicilio.isPresent()){
            return ResponseEntity.notFound().build();
        }
        
        oDomicilio.get().setEstado(domicilioUpdate.getEstado());
 
        return ResponseEntity.status(HttpStatus.CREATED).body(domicilioRepository.save(oDomicilio.get()));
    }
}
