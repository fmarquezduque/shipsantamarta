package com.example.domsantamarta_backend.repository;

import java.util.Optional;

import com.example.domsantamarta_backend.entity.Mensajero;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MensajeroRepository extends JpaRepository<Mensajero, Long> {
    
    @Query("SELECT d FROM Mensajero d WHERE d.cedula = :cedula or d.placa_moto = :placa_moto or d.telefono =:telefono")
	Optional<Mensajero> findMensajeroByCedulaOrPlacaMotoOrTelefono(
			@Param("cedula") String cedula, 
			@Param("placa_moto") String placa_moto, 
			@Param("telefono") String telefono);

}
