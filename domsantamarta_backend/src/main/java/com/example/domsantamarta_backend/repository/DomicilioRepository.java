package com.example.domsantamarta_backend.repository;

import java.util.List;

import com.example.domsantamarta_backend.entity.Domicilio;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DomicilioRepository extends JpaRepository<Domicilio, Long>{
    
    @Query("SELECT d,m.nombre, m.telefono FROM Domicilio d INNER JOIN Mensajero m ON d.mensajero = m.id  WHERE :estado IS NULL OR d.estado = :estado")
	List<Domicilio> findDomiciliosByEstado(
			@Param("estado") Boolean estado);
	
}
