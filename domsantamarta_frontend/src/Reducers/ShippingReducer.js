import {
    SHIPPING_LIST_SUCCESS, SHIPPING_LIST_ERR,
    SHIPPING_ID_SUCCESS, SHIPPING_ID_ERR,
    SHIPPING_UPDATE_SUCCESS, SHIPPING_UPDATE_ERR,
    SHIPPING_CREATE_SUCCESS, SHIPPING_CREATE_ERR

} from "../Constants/ShippingConstants";


function listShippingsReducer(state = { shippings : [] }, action){
    
    switch (action.type) {
        case SHIPPING_LIST_SUCCESS:
            return { shippings: action.payload };
        case SHIPPING_LIST_ERR:
            return { err: action.payload };
        default:
            return state;
    }
} 

function listShippingReducer(state = { shipping : {} }, action){
    
    switch (action.type) {
        case SHIPPING_ID_SUCCESS:
            return { shipping: action.payload };
        case SHIPPING_ID_ERR:
            return { err: action.payload };
        default:
            return state;
    }
} 

function createShippingReducer(state = { shipping : {} }, action){

    switch (action.type) {
        case SHIPPING_CREATE_SUCCESS:
            return { shipping: action.payload };
        case SHIPPING_CREATE_ERR:
            return { error: action.payload };
        default:
            return state;
    }
}

function updateShippingReducer(state = { shipping : {} }, action){

    switch (action.type) {
        case SHIPPING_UPDATE_SUCCESS:
            return { shipping: action.payload };
        case SHIPPING_UPDATE_ERR:
            return { error: action.payload };
        default:
            return state;
    }
}

export {
    listShippingsReducer,
    listShippingReducer,
    createShippingReducer,
    updateShippingReducer,
};