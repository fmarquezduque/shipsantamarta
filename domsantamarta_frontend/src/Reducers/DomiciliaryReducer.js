import {
    DOMICILIARY_LIST_SUCCESS, DOMICILIARY_LIST_ERR,
    DOMICILIARY_UPDATE_SUCCESS, DOMICILIARY_UPDATE_ERR,
    DOMICILIARY_CREATE_SUCCESS, DOMICILIARY_CREATE_ERR,
    DOMICILIARY_ID_SUCCESS, DOMICILIARY_ID_ERR
    

} from "../Constants/DomiciliaryConstants";


function listDomiciliariesReducer(state = { domiciliaries : [] }, action){
    
    switch (action.type) {
        case DOMICILIARY_LIST_SUCCESS:
            return { domiciliaries: action.payload };
        case DOMICILIARY_LIST_ERR:
            return { err: action.payload };
        default:
            return state;
    }
} 

function listDomiciliaryReducer(state = { domiciliary : {} }, action){
    
    switch (action.type) {
        case DOMICILIARY_ID_SUCCESS:
            return { domiciliary: action.payload };
        case DOMICILIARY_ID_ERR:
            return { err: action.payload };
        default:
            return state;
    }
} 

function createDomiciliaryReducer(state = { domiciliary : {} }, action){

    switch (action.type) {
        case DOMICILIARY_CREATE_SUCCESS:
            return { domiciliary: action.payload };
        case DOMICILIARY_CREATE_ERR:
            return { error: action.payload };
        default:
            return state;
    }
}

function updateDomiciliaryReducer(state, action){

    switch (action.type) {
        case DOMICILIARY_UPDATE_SUCCESS:
            return { domiciliary: action.payload };
        case DOMICILIARY_UPDATE_ERR:
            return { error: action.payload };
        default:
            return state;
    }
}

export {
    listDomiciliariesReducer,
    createDomiciliaryReducer,
    updateDomiciliaryReducer,
    listDomiciliaryReducer,
};