import {
    LOGIN_SUCCESS, LOGIN_ERR,
    UPDATE_PASSWORD_SUCCESS, UPDATE_PASSWORD_ERR,
    LOGOUT
} from '../Constants/UserConstants';


function loginReducer(state = {}, action){

    switch (action.type) {
        case LOGIN_SUCCESS:
          return { userInfo: action.payload };
        case LOGIN_ERR:
          return { error: action.payload };
        case LOGOUT:
          return {};
        default: return state;
      }

}

function updatePasswordReducer(state = {}, action){

    switch (action.type) {
        case UPDATE_PASSWORD_SUCCESS:
          return {  userInfo: action.payload };
        case UPDATE_PASSWORD_ERR:
          return {  error: action.payload };
        default: return state;
    }

}

export { loginReducer, updatePasswordReducer  };
