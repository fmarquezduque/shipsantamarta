
export const SHIPPING_LIST_SUCCESS = 'SHIPPING_LIST_SUCCESS';
export const SHIPPING_LIST_ERR = 'SHIPPING_LIST_ERR';

export const SHIPPING_ID_SUCCESS = 'SHIPPING_ID_SUCCESS';
export const SHIPPING_ID_ERR = 'SHIPPING_ID_ERR';

export const SHIPPING_UPDATE_SUCCESS = 'SHIPPING_UPDATE_SUCCESS';
export const SHIPPING_UPDATE_ERR = 'SHIPPING_UPDATE_ERR';

export const SHIPPING_CREATE_SUCCESS = 'SHIPPING_CREATE_SUCCESS';
export const SHIPPING_CREATE_ERR = 'SHIPPING_CREATE_ERR';
