import React from 'react';
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import {MarkerIconRed, MarkerIconGreen} from './IconMap.js';

import 'leaflet/dist/leaflet.css';

export default function MapOne({point}){

    return (
  
        <div className="center" >
            <MapContainer 
                center={{lat:point.destinatario_latitud, lng:point.destinatario_longitud}} 
                zoom={1} 
            > 
                <TileLayer
                    attribution='Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                    <Marker
                        position = {{lat:point.destinatario_latitud, lng:point.destinatario_longitud}} 
                        icon = {point.estado ?  MarkerIconGreen : MarkerIconRed }  
                    >
                        <Popup>
                            { point.destinatario_nombre } 
                        </Popup>
                    </Marker>
                    <Marker
                        position = {{lat:point.solicitante_latitud, lng:point.solicitante_longitud}} 
                        icon = {point.estado ?  MarkerIconGreen : MarkerIconRed }  
                    >
                        <Popup>
                            { point.solicitante_nombre  }
                        </Popup>
                    </Marker>
            </MapContainer>

        </div>
       
        
    )
}
