import React, { useState, useEffect } from 'react';
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import {MarkerIconRed, MarkerIconGreen} from './IconMap.js';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import DetailsShipping from '../Shipping/DetailsShipping';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import { list } from '../../Actions/ShippingActions';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

import 'leaflet/dist/leaflet.css';

const useStyles = makeStyles((theme) => ({
    root: {
      '& > *': {
        margin: theme.spacing(.5),
      },
      padding:{
        padding: theme.spacing(0.5)
      }
    },
    padding: {
        padding: theme.spacing(3.5),
    },
    float:{
        float:'right'
    } 

}));

export default function Mapcity(){

    const classes = useStyles();

    const [shippingDetail, setShippingDetail]  = useState(false);
    const [shipping, setShipping] = useState({});
    const [category, setCategory] = useState(2); 
    const mapsList = useSelector((state) => state.listShippings);
    const { shippings, error } = mapsList;
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(list(category));
    }, [category]);

    const handlerClickMarker = (coord) => {
        setShipping(coord);
        setShippingDetail(true);
    }

    return (
        <Container component="main">
            <CssBaseline />
            <Grid container>
                <Grid item xs={10}>
                    <Box className={classes.root} m={2}>
                        <Button variant="contained" onClick = { e => setCategory(2) }>Todos</Button>
                        <Button variant="contained" onClick = { e => setCategory(1) }>Pendientes</Button>
                        <Button variant="contained" onClick = { e => setCategory(0) }>Cerrados</Button>
                    </Box>
                </Grid>

                <Box m={2}>
                    <Button variant="contained"><Link variant="contained" to = "/">Tabla</Link></Button>
                </Box>    
            </Grid>
            <div className={'center'}>
                {error && <div>{error}</div>}
                <MapContainer 
                    center={{lat:'11.2257948', lng:'-74.187273'}} 
                    zoom={12} 
                > 
                    <TileLayer
                        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    {
                        shippings && shippings.map( (coord) => (
                            <Marker
                                key = { coord.id }
                                data = { coord }  
                                eventHandlers={{
                                    click: (e) => { handlerClickMarker(e.target.options.data); }}}
                                position = {{lat:coord.destinatario_latitud, lng:coord.destinatario_longitud}} 
                                icon = {coord.estado ?  MarkerIconGreen : MarkerIconRed }  
                            >
                                <Popup>
                                   {coord.id } - { coord.descrip_paquete }
                                </Popup>
                            </Marker>
                        ))
                    }
                   
                </MapContainer>

                {shippingDetail && <DetailsShipping coord = {shipping} /> }
            </div>
        </Container>
        
    )
}
