import React, { useEffect, useState } from 'react'
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { list } from '../Actions/ShippingActions';
import { useSelector, useDispatch } from 'react-redux';
import Row from './Collapse/Row';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    root: {
      '& > *': {
        margin: theme.spacing(.5),
      },
      padding:{
        padding: theme.spacing(0)
      }
    },
    padding: {
        padding: theme.spacing(3.5),
    },
    float:{
        float:'right'
    } 

}));



export default function Home() {
        
    const classes = useStyles();
    const [category, setCategory] = useState(2);
    const listShippings = useSelector((state) => state.listShippings);
    const { shippings, err } = listShippings;
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(list(category));
    }, [category]);
    
    
    return (
        <Container component="main">
            <CssBaseline />
            <div className={classes.padding} >
                <Box m={3}>
                    <Button className={classes.float} variant="contained"><Link to = "/create/shipping" variant="contained">+ Crear Domicilio</Link></Button>
                </Box>
                <Grid container>
                    <Typography component="h1" variant="h5">
                        Domicilios solicitados
                    </Typography>
                    <Grid item xs={10}>
                        <Box className={classes.root} m={2}>
                            <Button variant="contained" onClick = { e => setCategory(2) }>Todos</Button>
                            <Button variant="contained" onClick = { e => setCategory(1) }>Pendientes</Button>
                            <Button variant="contained" onClick = { e => setCategory(0) }>Cerrados</Button>
                        </Box>
                    </Grid>
                    <Box m={2}>
                        <Button ariant="contained"><Link to = "/maps" variant="contained">Ver Mapa</Link></Button>
                    </Box>    
                </Grid>
                <div style={{ paddingBottom:'14%'}}>
                    {err && <div>{err}</div>}
                    <TableContainer component={Paper}>
                        <Table aria-label="collapsible table">
                            <TableHead>
                                <TableRow>
                                    <TableCell />
                                    <TableCell>ID</TableCell>
                                    <TableCell align="right">Solicitante</TableCell>
                                    <TableCell align="right">Estado</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    shippings && shippings.map((shipping) => (
                                        <Row key={shipping.id} row={shipping} />
                                    ))   
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </div>
            </div>
        </Container>
    )
}
