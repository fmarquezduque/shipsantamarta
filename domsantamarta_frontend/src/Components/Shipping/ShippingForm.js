import React, { useState, useEffect } from 'react';
import Alert from '@material-ui/lab/Alert';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import InputLabel from '@material-ui/core/InputLabel';
import { position } from '../../Utils';
import { list } from '../../Actions/DomiciliaryActions';
import { create } from '../../Actions/ShippingActions';
import { useSelector, useDispatch } from 'react-redux';


const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  title: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(2),
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));



export default function ShippingForm(props) {

  const classes = useStyles();

  const [name, setName] = useState('');
  const [latitud, setLatitud] = useState('');
  const [longitud, setLongitud] = useState('');
  const [celular, setCelular] = useState('');
  const [hora, setHora] = useState('');
  const [name2, setName2] = useState('');
  const [latitud2, setLatitud2] = useState('');
  const [longitud2, setLongitud2] = useState('');
  const [celular2, setCelular2] = useState('');
  const [packageDescription, setPackageDescription] = useState('');
  const [domiciliary, setDomiciliary] = useState(1);
  
  const [positionValid, setPositionValid] = useState(true);

  const  createShipping  = useSelector((state) => state.createShipping);
  const { shipping, err } = createShipping;

  const domiciliariesList = useSelector((state) => state.domiciliariesList);
  const { domiciliaries, err2 } = domiciliariesList;

  const dispatch = useDispatch();
  
  const redirect = props.location.search ? props.location.search.split("=")[1] : '/';

  useEffect(() => {
    
    return () => {
      if (shipping) {
        props.history.replace(redirect);
      }
    };
  }, [shipping]);

  const handleCancel = (e) =>{
    props.history.replace(redirect);
  }

  const submitHandler = (e) => {
    e.preventDefault();
    if( position(longitud) && position(latitud) &&  position(longitud2) && position(latitud2) ){
      setPositionValid(true);
      dispatch(create(name, latitud , longitud, celular, hora, name2, latitud2,longitud2, celular2, packageDescription, domiciliary));
    }else{
      setPositionValid(false);
    }
    
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        {  !positionValid && (<Alert variant="filled" severity="error">
                                Una o varias de las posiciones geograficas son incorrectas! - corrigela(s) 
                                </Alert>) 
        }
        <Typography component="h1" variant="h5">
          Agregar Domicilio
        </Typography>
        
        <form className={classes.form} noValidate onSubmit = { submitHandler }>
          
          <Grid container>
            <Typography component="subtitle1" align="left">
                Información del Solicitante
            </Typography>
          </Grid>
          <Grid container spacing={2} className={classes.title}  >
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="name"
                label="Nombre"
                name="name"
                onChange = { (e) => setName(e.target.value) }
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="latitud"
                label="Latitud"
                type="float"
                name="latitud"
                onChange = { (e) => setLatitud(e.target.value) }
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                type="float"
                name="longitud"
                label="Longitud"
                id="longitud"
                onChange = { (e) => setLongitud(e.target.value) }
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                name="celular"
                variant="outlined"
                required
                fullWidth
                id="celular"
                label="Celular"
                autoFocus
                onChange = { (e) => setCelular(e.target.value) }
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                id="time"
                label="Hora recoger"
                type="time"
                defaultValue="06:30"
                required
                fullWidth
                autoFocus
                InputLabelProps={{
                  shrink: true,
                }}
                inputProps={{
                  step: 300, // 5 min
                }}
                onChange = { (e) => setHora(e.target.value) }
                />
            </Grid>
          </Grid>
          <Grid container>
            <Typography component="subtitle1" align="left">
                Información del destinatario
            </Typography>
          </Grid>
          <Grid container spacing={2} className={classes.title} >
            <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  id="name2"
                  label="Nombre"
                  name="name2"
                  onChange = { (e) => setName2(e.target.value) }
                />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="latitud2"
                label="Latitud"
                type="float"
                name="latitud2"
                onChange = { (e) => setLatitud2(e.target.value) }
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                type="float"
                name="longitud2"
                label="Longitud"
                id="longitud2"
                onChange = { (e) => setLongitud2(e.target.value) }
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                name="celular2"
                variant="outlined"
                required
                fullWidth
                id="celular2"
                label="Celular"
                autoFocus
                onChange = { (e) => setCelular2(e.target.value) }
              />
            </Grid>
          </Grid>
          <Grid container>
            <Typography  component="subtitle1" align="left">
                Información de entrega
            </Typography>
          </Grid>
          <Grid container spacing={2} className={classes.title} >
            <Grid item xs={12}>
              <TextField
                name="package_description"
                variant="outlined"
                required
                fullWidth
                id="package_description"
                label="Descripción del paquete"
                autoFocus
                onChange = { (e) => setPackageDescription(e.target.value) }
              />
            </Grid>
          </Grid>
          <div>
              <InputLabel htmlFor="age-native-simple">Domiciliario</InputLabel>
              <Select
                native
                onChange={(e) => setDomiciliary(e.target.value)}
              >
                <option aria-label="None" value={0} />
                {
                domiciliaries.map((dom) => ((
                  <option value={dom.id}>{dom.nombre}</option>
                )))
                }
              </Select>
          </div>
          <Grid  container spacing={2} style={{paddingBottom:'5rem'}}>
            <Grid item xs={12} sm={6}>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="warning"
                    onClick = { handleCancel }
                    className={classes.submit}
                >
                Cancelar
                </Button>
            </Grid>
            <Grid item xs={12} sm={6}>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                >
                Guardar
                </Button>
            </Grid>
         </Grid>
        </form>
      </div>
    </Container>
  );
}