import React, { useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { detail } from '../../Actions/DomiciliaryActions';
import { useSelector, useDispatch } from 'react-redux';

export default function DetailsShipping({coord}){

    const domList = useSelector((state) => state.listDomiciliary);
    const { domiciliary, err } = domList;
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(detail(coord.mensajero));
    }, [coord.mensajero]);

   
    return (
        <div style={{paddingTop:'4%', paddingBottom:'14%'}}>
            <Typography component="h1" variant="h5">
                Información del domicilio #{coord.id}
                <hr/>
            </Typography>
            <Grid container spacing={2}>
                <Grid item xs={6}>
                    <Typography component="h1" variant="h5" >
                        Información del solicitante
                    </Typography>
                    <Grid>
                        Nombre: {coord.solicitante_nombre}<br/>
                        Celular: {coord.solicitante_telefono}<br/>
                        Dirección: {coord.solicitante_latitud},{coord.solicitante_longitud}<br/>
                        Hora escogida: {coord.hora}<br/>
                        Observación: {coord.descrip_paquete}
                    </Grid>
                </Grid>
                <Grid item xs={6}>
                    <Typography component="h1" variant="h5">
                        Información del Destinatario
                    </Typography>
                    <Grid>
                        Nombre: {coord.destinatario_nombre}<br/>
                        Celular: {coord.destinatario_telefono}<br/>
                        Dirección: {coord.destinatario_latitud},{coord.destinatario_longitud}<br/>
                        Hora entrega: {coord.hora}<br/>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Typography component="h1" variant="h5">
                        Información del mensajero
                    </Typography>
                    <Grid>
                        Nombre: {domiciliary.nombre} <br/>
                        Celular: {domiciliary.telefono}
                    </Grid>
                </Grid>
            </Grid>
        </div>
    )
}
