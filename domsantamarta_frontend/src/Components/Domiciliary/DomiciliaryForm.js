import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { create } from '../../Actions/DomiciliaryActions';
import { useSelector, useDispatch } from 'react-redux';


const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function DomiciliaryForm(props) {
  const classes = useStyles();

  const [nombre, setNombre] = useState('');
  const [direccion, setDireccion] = useState('');
  const [cedula, setCedula] = useState('');
  const [telefono, setTelefono] = useState('');
  const [placaMoto, setPlacaMoto] = useState('');

  const  domiciliaryCreate  = useSelector((state) => state.createDomiciliary);
  const { domiciliary, error } = domiciliaryCreate;

  console.log(error);
  
  const dispatch = useDispatch();

  const redirect = props.location.search ? props.location.search.split("=")[1] : '/domiciliary';
  
  useEffect(() => {
    return () => {
      if (domiciliary) {
        props.history.replace(redirect);
      }
    };
  }, [domiciliary]);

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(create(nombre, direccion, cedula, telefono, placaMoto));
  }

  const handleCancel= () => props.history.replace(redirect);

  return (
    
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
            Agregar nuevo miembro al equipo
        </Typography>
        <form className={classes.form} noValidate onSubmit = {submitHandler } >
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="name"
                label="Nombre"
                name="name"
                onChange = { (e) => setNombre(e.target.value) }
              />
            </Grid>
            <Grid item xs={12} sm={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                type="float"
                name="direccion"
                onChange = { (e) => setDireccion(e.target.value) }
                label="Direccion"
                id="direccion"
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="cedula"
                label="Cédula"
                name="cedula"
                onChange = { (e) => setCedula(e.target.value) }
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="telefono"
                label="Telefono"
                type="text"
                id="telefono"
                onChange = { (e) => setTelefono(e.target.value) }
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="placamoto"
                onChange = { (e) => setPlacaMoto(e.target.value) }
                label="Placa moto"
                type="text"
                id="placamoto"
              />
            </Grid>
          </Grid>
          <Grid container spacing={2} className={classes.title}>
            <Grid item xs={12} sm={6}>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    onClick={ handleCancel }
                    color="warning"
                    className={classes.submit}
                >
                Cancelar
                </Button>
            </Grid>
            <Grid item xs={12} sm={6}>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                >
                Guardar
                </Button>
            </Grid>
         </Grid>
          
        </form>
      </div>
    </Container>
  );
}