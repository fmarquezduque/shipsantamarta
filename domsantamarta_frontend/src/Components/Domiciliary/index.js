import React, { useEffect } from 'react';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import { DataGrid } from '@material-ui/data-grid';
import { list } from '../../Actions/DomiciliaryActions';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    root: {
      '& > *': {
        margin: theme.spacing(.5),
      },
      padding:{
        padding: theme.spacing(3.5)
      }
    },
    padding: {
        padding: theme.spacing(3.5),
    },
    float:{
        float:'right'
    } 
}));

const columns = [
    { field: 'id', headerName: '#ID', width: 130 },
    { field: 'nombre', headerName: 'Nombre', width: 150 },
    { field: 'cedula', headerName: '# Cédula', width: 150 },
    { field: 'telefono', headerName: '# Celular', width: 150 },
    { field: 'placa_moto', headerName: '# Placa', width: 130 },
    { field: 'direccion', headerName: 'Dirección', width: 130 },
];
      

export default function Domiciliary() {

    const classes = useStyles();
    const domiciliariesList = useSelector((state) => state.domiciliariesList);
    const { domiciliaries, err } = domiciliariesList;
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(list());
    }, []);
    
    return (
        <Container component="main">
            <CssBaseline />
            <div className={classes.padding}>
                <Box m={3}>
                    <Button className={classes.float}  variant="contained"><Link variant="contained" to = "/create/domiciliary">+ Crear Mensajero</Link></Button>
                </Box>
                <Grid container >
                    <Typography component="h1" variant="h5">
                        Nuestro equipo de trabajo
                    </Typography>
                </Grid>
                <div style={{ height: 400, width: '90%', paddingBottom:'2rem'}} >
                    { domiciliaries ? <DataGrid rows={domiciliaries} columns={columns} pageSize={5} />
                    : <DataGrid rows={[]} columns={columns} pageSize={5} />
                    }
                </div>
                {
                    err && (
                        <Typography component="h1" variant="h5">
                            {err}
                        </Typography>
                    )
                }
                        
            </div>
        </Container>
    )
}
