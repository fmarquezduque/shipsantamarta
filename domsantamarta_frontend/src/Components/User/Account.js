import React, { useState } from 'react'
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import useStyles from './style';


export default function Account(){

    const classes = useStyles();

    const [password, setPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');

    const submitHandler = (e) => {
      e.preventDefault();
      console.log(password, newPassword, confirmPassword);

    }
    return(
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Typography component="h1" variant="h5">
                    Cambia tu contraseña
                </Typography>
                <p>Para cambiar tu contrsaseña debes completar el siguiente formulario.</p>
                <form className={classes.form} noValidate onSubmit = { submitHandler }>
                  <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="current_password"
                    onChange = { (e) => setPassword(e.target.value) }
                    label="Contraseña actual"
                    type="password"
                    id="current_password"
                    autoComplete="current-password"
                  />
                  <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="new_password"
                    onChange = { (e) => setNewPassword(e.target.value) }
                    label="Nueva contraseña"
                    type="password"
                    id="new_password"
                    autoComplete="current-password"
                  />
                  <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="confirm_password"
                    onChange = { (e) => setConfirmPassword(e.target.value) }
                    label="Confirmar contraseña"
                    type="password"
                    id="confirm_password"
                    autoComplete="current-password"
                  />
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                  >
                    Guardar contraseña
                  </Button>
                </form>
            </div>
        </Container>
    )

}