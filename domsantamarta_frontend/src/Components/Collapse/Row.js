import React, {  useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import Button from '@material-ui/core/Button';
import TableRow from '@material-ui/core/TableRow';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import DetailsShipping from '../Shipping/DetailsShipping';
import { green } from '@material-ui/core/colors';
import { createTheme, ThemeProvider } from '@material-ui/core/styles';
import MapOne from '../Map/MapOne';
import { update } from '../../Actions/ShippingActions';

const theme = createTheme({
    palette: {
      primary: green,
    },
});

function Row(props) {
    const { row } = props;
    const [open, setOpen] = useState(false);
    const [estado, setEstado] = useState(2); // para controlar el llamado a update

    const updateShipping = useSelector(state => state.updateShipping);
    const { shipping, error } = updateShipping;
    const dispatch = useDispatch();

    useEffect(() => {
        if(estado!==2){
            dispatch(update(row.id, estado));
        }
    }, [shipping, estado])


    return (
      <>
       {error && <div>{error}</div>}
        <TableRow>
            <TableCell>
                <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                    {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                </IconButton>
            </TableCell>
            <TableCell component="th" scope="row">
                {row.id}
            </TableCell>
            <TableCell align="right">{row.solicitante_nombre}</TableCell>
            <TableCell align="right">
                {   row.estado ? ( 
                        <ThemeProvider theme={theme}>
                            <Button  
                                size="small" 
                                variant="contained"
                                color="primary" 
                                onClick = { e => setEstado(!row.estado) }  
                                variant="contained"
                            >
                                Pendiente
                            </Button>
                        </ThemeProvider>
                    )   : 
                    (
                        <Button  size="small" color="secondary" onClick = { e => setEstado(!row.estado) }  variant="contained">Cerrado</Button>
                    )
                }
            </TableCell>
        </TableRow>
        <TableRow>
            <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                <Collapse in={open} timeout="auto" unmountOnExit>
                    <Box margin={1}>
                        <Table size="small" aria-label="purchases">
                            <TableBody>
                                <TableRow>
                                    <TableCell component="th" scope="row">
                                       <DetailsShipping coord = { row } />
                                       <MapOne point = { row } /> 
                                    </TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>
                    </Box>
                </Collapse>
            </TableCell>
        </TableRow>

      </>
      )
  }

export default Row;