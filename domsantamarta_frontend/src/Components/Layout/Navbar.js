import React from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import { logout } from '../../Actions/UserActions';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import Typography from '@material-ui/core/Typography';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';


const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },

}));

export default function Navbar() {

  const classes = useStyles();
  const dispatch = useDispatch();

  const userSignin = useSelector(state => state.userSignin);
  const { userInfo } = userSignin;

  const history = useHistory();

  const handleLogout = () => {
    dispatch(logout());
    history.replace('/login');
  }

  return (
    <>
      <AppBar position="static" color="second">
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="primary"
            aria-label="open drawer"
          >
            <NavLink  to="/" exact >DOMSantaMarta</NavLink>
          </IconButton>
          
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            <IconButton color="inherit">
              <Badge color="secondary">
                <NavLink to="/" exact activeClassName = "active" >Domicilios</NavLink>
              </Badge>
            </IconButton>
            <IconButton  color="inherit">
              <Badge color="secondary">
                <NavLink to="/domiciliary" exact activeClassName = "active" >Mensajeros</NavLink>
              </Badge>
            </IconButton>
            <IconButton  color="inherit">
              <Badge color="secondary">
              <Typography component="h1" variant="h5">
                Hola, { userInfo.username}
              </Typography>
              </Badge>
            </IconButton>
            <IconButton
              edge="end"
              aria-haspopup="true"
              color="inherit"
            >
              <ExitToAppIcon onClick={handleLogout}>

             </ExitToAppIcon>
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      
    </>
  );
}
