import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';

export default function Footer() {
    return (
        <AppBar position="fixed" color="second" style={{top: "auto", bottom: 0}}>
          <Container maxWidth="md">
            <Toolbar>
              <Typography variant="body1" color="inherit">
                DOMSANTAMARTA © 2021 
              </Typography>
            </Toolbar>
          </Container>
        </AppBar>
    )
}