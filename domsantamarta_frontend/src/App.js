import './App.css';
import {
  BrowserRouter as Router,
  Switch,
} from "react-router-dom";
import Login from './Components/User/Login';
import Dashboard from './routes/Dashboard';
import { useSelector } from 'react-redux';
import { PrivateRoutes } from './routes/PrivateRoutes';
import { PublicRoutes } from './routes/PublicRoutes';

function App() {

  const user = useSelector((state) => state.userSignin);
  const { userInfo } = user;
  
  return (
    <Router>
        <div>
          <Switch>
              <PublicRoutes 
                  exact 
                  path = "/login" 
                  component = { Login }
                  isAuth = {  userInfo ? true : false }  
              />
              <PrivateRoutes
                  isAuth = { userInfo ? true : false } 
                  path = "/" 
                  component = { Dashboard } 
              />
          </Switch>
        </div>
    </Router>
  );
}

export default App;
