
// Función para validar la latitud y longitud

const position = (pos) => {
    let r = new RegExp('[+-]?([0-9]+[.])[0-9]+');
    return r.test(pos);
}

export { position };