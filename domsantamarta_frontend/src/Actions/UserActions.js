import Axios from "axios";
import Cookie from 'js-cookie';
import {
    LOGIN_SUCCESS, LOGIN_ERR,
    UPDATE_PASSWORD_SUCCESS, UPDATE_PASSWORD_ERR,
    LOGOUT
} from '../Constants/UserConstants';


const login  = (username, password) => async (dispatch) => {

    try {
        const { data } = await Axios.post("/api/auth/signin", { username, password });
        dispatch({type: LOGIN_SUCCESS, payload: data});
        Cookie.set('userInfo', JSON.stringify(data));
    } catch (error) {
        dispatch({ type: LOGIN_ERR, payload: error.message });
    }
}

const updatePassword =  ({ id, username, email, password }) => async (dispatch, getState) => {
    
    try {
        const {
            userSignin: { userInfo },
          } = getState();
        const { data } = await Axios.put("/api/users" + id, { username, email, password },{
            headers: {
              Authorization: 'Bearer ' + userInfo.accessToken
            }
        });
        dispatch({ type: UPDATE_PASSWORD_SUCCESS, payload: data });
    } catch (error) {
        dispatch({ type: UPDATE_PASSWORD_ERR, paylaod: error.message });
    }
}

const logout =  () => async (dispatch) => {
    Cookie.remove("userInfo");
    dispatch({ type: LOGOUT });
}

export { login, logout, updatePassword };