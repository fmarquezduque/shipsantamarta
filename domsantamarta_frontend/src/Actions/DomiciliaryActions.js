import Axios from "axios";
import {
    DOMICILIARY_LIST_SUCCESS, DOMICILIARY_LIST_ERR,
    DOMICILIARY_UPDATE_SUCCESS, DOMICILIARY_UPDATE_ERR,
    DOMICILIARY_CREATE_SUCCESS, DOMICILIARY_CREATE_ERR,
    DOMICILIARY_ID_SUCCESS, DOMICILIARY_ID_ERR

} from "../Constants/DomiciliaryConstants";


const list = () => async (dispatch, getState) => {

    try {
        const {
            userSignin: { userInfo },
          } = getState();
        const { data } = await Axios.get("api/mensajeros",{
            headers: {
              Authorization: 'Bearer ' + userInfo.accessToken
            }
        });
        dispatch({ type: DOMICILIARY_LIST_SUCCESS, payload: data });
    } catch (error) {
        dispatch({ type: DOMICILIARY_LIST_ERR, payload: error.message });
    }
}

const detail = (id) => async (dispatch, getState) => {

    try {
        const {
            userSignin: { userInfo },
          } = getState();
        const { data } = await Axios.get("api/mensajeros/"+id, {
            headers: {
              Authorization: 'Bearer ' + userInfo.accessToken
            }
        });
        dispatch({ type: DOMICILIARY_ID_SUCCESS, payload: data });
    } catch (error) {
        dispatch({ type: DOMICILIARY_ID_ERR, payload: error.message });
    }
}

const create = ( 
    nombre,
    direccion,
    cedula,
    telefono,
    placa_moto
     ) => async (dispatch, getState) => {
    
    try {
        const {
            userSignin: { userInfo },
          } = getState();
        const { data } = await Axios.post("/api/mensajeros", { 
        nombre,
        direccion,
        cedula,
        telefono,
        placa_moto},
        {
            headers: {
              Authorization: 'Bearer ' + userInfo.accessToken
            }
        });

        dispatch({ type: DOMICILIARY_CREATE_SUCCESS, payload: data });

    } catch (error) {
        dispatch({ type: DOMICILIARY_CREATE_ERR, payload: error.message });
    }
}

const update =  (id,
    nombre,
    direccion,
    cedula,
    telefono,
    placa_moto
    ) => async (dispatch, getState) => {
    
    try {
        const {
            userSignin: { userInfo },
          } = getState();
        const { data } = await Axios.put("/api/mensajeros/" + id, { 
        nombre,
        direccion,
        cedula,
        telefono,
        placa_moto},
        {
            headers: {
              Authorization: 'Bearer ' + userInfo.accessToken
            }
        });

        dispatch({ type: DOMICILIARY_UPDATE_SUCCESS, payload: data });
    } catch (error) {
        dispatch({ type: DOMICILIARY_UPDATE_ERR, payload: error.message });
    }
}



export { list, create, update, detail };