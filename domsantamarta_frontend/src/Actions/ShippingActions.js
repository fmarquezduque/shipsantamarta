import Axios from "axios";

import {

    SHIPPING_LIST_SUCCESS,SHIPPING_LIST_ERR,
    SHIPPING_ID_SUCCESS, SHIPPING_ID_ERR,
    SHIPPING_UPDATE_SUCCESS, SHIPPING_UPDATE_ERR,
    SHIPPING_CREATE_SUCCESS, SHIPPING_CREATE_ERR

} from '../Constants/ShippingConstants';

// Listar los domicilios de acuerdo a sortOrder ( todo: all, pendientes: open y cerrados: close )
const list = (estado) => async (dispatch, getState) => {

    try {
        const {
            userSignin: { userInfo },
          } = getState();
        
        if(estado === 2){
            const { data } = await Axios.get("/api/domicilios", {
                headers: {
                  Authorization: 'Bearer ' + userInfo.accessToken
                }
            });
            dispatch({ type: SHIPPING_LIST_SUCCESS, payload: data });
        }else{
            const { data } = await Axios.get("/api/domicilios?state="+estado, {
                headers: {
                  Authorization: 'Bearer ' + userInfo.accessToken
                }
            });
            dispatch({ type: SHIPPING_LIST_SUCCESS, payload: data });
        }

    } catch (error) {
        dispatch({ type: SHIPPING_LIST_ERR, payload: error.message });

    }
}

const create = ( 
    solicitante_nombre,
    solicitante_latitud,
    solicitante_longitud,
    solicitante_telefono,
    hora,
    destinatario_nombre,
    destinatario_latitud,
    destinatario_longitud,
    destinatario_telefono,
    descrip_paquete,
    mensajero ) => async (dispatch, getState) => {
    
    try {
        const {
            userSignin: { userInfo },
          } = getState();
        const { data } = await Axios.post("/api/domicilios", {
        solicitante_nombre,
        solicitante_latitud,
        solicitante_longitud,
        solicitante_telefono,
        hora,
        destinatario_nombre,
        destinatario_latitud,
        destinatario_longitud,
        destinatario_telefono,
        descrip_paquete,
        mensajero },
        {
            headers: {
              Authorization: 'Bearer ' + userInfo.accessToken
            }
        });

        dispatch({ type: SHIPPING_CREATE_SUCCESS, payload: data });

    } catch (error) {
        dispatch({ type: SHIPPING_CREATE_ERR, payload: error.message });
    }
}

const detail =  (id) => async (dispatch, getState) => {
    
    try {
        const {
            userSignin: { userInfo },
          } = getState();
        const { data } = await Axios.get("/api/domicilios/" + id,
        {
            headers: {
              Authorization: 'Bearer ' + userInfo.accessToken
            }
        });
        dispatch({ type: SHIPPING_ID_SUCCESS, payload: data });

    } catch (error) {
        dispatch({ type: SHIPPING_ID_ERR, payload: error.message });
    }
}

const update = ( id, estado ) => async (dispatch, getState) => {

    try {
        const {
            userSignin: { userInfo },
          } = getState();
        const { data } = await Axios.put("/api/domicilios/"+id, { estado },
        {   
            headers: {
              Authorization: 'Bearer ' + userInfo.accessToken
            }
        });

        dispatch({ type: SHIPPING_UPDATE_SUCCESS, payload: data });

    } catch (error) {
        dispatch({ type: SHIPPING_ID_ERR, payload: error.message });
    }
}


export { list, create, detail, update };