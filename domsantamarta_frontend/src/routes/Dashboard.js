import {
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import Navbar from '../Components/Layout/Navbar';
import Footer from '../Components/Layout/Footer';
import MapCity from '../Components/Map/Map';
import Domiciliary from '../Components/Domiciliary/index';
import Account from '../Components/User/Account';
import DomiciliaryForm from '../Components/Domiciliary/DomiciliaryForm';
import ShippingForm from '../Components/Shipping/ShippingForm';
import Home from '../Components/index'

function Dashboard() {
  return (
    <>
        <Navbar/>
        <div>
          <Switch>
              <Route exact path = "/" component = { Home } />
              <Route exact path = "/domiciliary" component = { Domiciliary } />
              <Route exact path = "/create/domiciliary" component = { DomiciliaryForm } />
              <Route exact path = "/create/shipping" component = { ShippingForm } />
              <Route exact path = "/account" component = { Account } />
              <Route exact path = "/maps" component = { MapCity } />
              <Redirect to = "/" />
          </Switch>
        </div>
        <Footer/>
    </>
  );
}

export default Dashboard;
