import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import Cookie from 'js-cookie';

import {
    listDomiciliariesReducer,
    createDomiciliaryReducer,
    listDomiciliaryReducer,
    //updateDomiciliaryReducer
} from './Reducers/DomiciliaryReducer';

import {
    listShippingsReducer,
    //listShippingReducer,
    createShippingReducer,
    updateShippingReducer,

} from './Reducers/ShippingReducer';

import {
    loginReducer, 
    updatePasswordReducer
} from './Reducers/UserReducer';

const userInfo = Cookie.getJSON('userInfo') || false;

const initialState = {
    userSignin: { userInfo }
};

const reducer = combineReducers({

    userSignin: loginReducer,
    userUpdate: updatePasswordReducer,
    domiciliariesList: listDomiciliariesReducer,
    createDomiciliary: createDomiciliaryReducer,
    listDomiciliary: listDomiciliaryReducer,
    //updateDomiciliary: updateDomiciliaryReducer,
    listShippings: listShippingsReducer,
    //listShipping: listShippingReducer,
    createShipping: createShippingReducer,
    updateShipping: updateShippingReducer,

});

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    reducer,
    initialState,
    composeEnhancer(applyMiddleware(thunk))
);

export default store;
